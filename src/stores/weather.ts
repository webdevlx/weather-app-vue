import axios from "axios";
import { defineStore } from "pinia";

interface Weather {
  main: string;
  description: string;
}

interface HourlyWeather {
  dt: number;
  main: {
    temp: number;
    feels_like: number;
  };
  weather: Weather[];
}

interface State {
  isLoading: boolean;
  city: string;
  currentWeather: null | {
    name: string;
    dt: number;
    timezone: number;
    main: {
      temp: number;
      feels_like: number;
      humidity: number;
      pressure: number;
    };
    wind: {
      speed: number;
    };
    weather: Weather[];
  };
  hourlyWeather: HourlyWeather[];
}

export const useWeatherStore = defineStore("WeatherStore", {
  state: (): State => ({
    isLoading: true,
    city: "Ташкент",
    currentWeather: null,
    hourlyWeather: [],
  }),
  actions: {
    getData() {
      if (this.city !== "") {
        axios
          .get(
            `https://api.openweathermap.org/data/2.5/weather?q=${this.city}&appid=11bbfba58b96a7ccb3864bc1e59bbd96&units=metric&lang=ru`
          )
          .then((response) => {
            this.currentWeather = response.data;
            axios
              .get(
                `https://api.openweathermap.org/data/2.5/forecast?q=${this.city}&appid=11bbfba58b96a7ccb3864bc1e59bbd96&units=metric&lang=ru`
              )
              .then((response) => {
                this.hourlyWeather = response.data.list;
                this.isLoading = false;
              });
          });
      }
    },
  },
});
